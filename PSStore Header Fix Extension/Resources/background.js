chrome.webRequest.onHeadersReceived.addListener(
  function(details) {
  	var r = [];
  	details.responseHeaders.forEach(function(s){
  		if(s.name.toLowerCase() != 'access-control-allow-origin'){
  			r.push(s);
  		}
  	});
  	r.push({name:'access-control-allow-origin',value:'*'});
    return {responseHeaders: r};
	},
  {
    urls: ["https://static.playstation.com/*.js","https://static.playstation.com/*.svg","https://static.playstation.com/*.json"],
  },
  ["blocking","responseHeaders", "extraHeaders"]
);